package com.branow;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingTest {

    private final int[] actual;
    private final int[] expected;


    public SortingTest(int[] actual, int[] expected) {
        this.actual = actual;
        this.expected = expected;
    }

    @Test
    public void testSorting() {
        Sorting.sort(actual);
        Assert.assertArrayEquals(expected, actual);
    }

    @Parameterized.Parameters
    public static Collection parameters() {
        return Arrays.asList(new Object[][]{
                {new int[]{}, new int[]{}},
                {new int[]{1}, new int[]{1}},
                {new int[]{5, 4, 7, 1}, new int[]{1, 4, 5, 7}},
                {new int[]{0, 8, 4, 2, -1, 7, 6, 2, -9, 3, 10}, new int[]{-9, -1, 0, 2, 2, 3, 4, 6, 7, 8, 10}}
        });
    }
}
