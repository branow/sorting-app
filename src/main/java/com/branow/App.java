package com.branow;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello)");
        int[] ar = toIntArray(args);
        System.out.println("Your array " + Arrays.toString(ar));
        Sorting.sort(ar);
        System.out.println("Sorted array " + Arrays.toString(ar));
    }

    public static int[] toIntArray(String[] args) {
        int[] array = new int[args.length];
        for (int i=0; i<array.length; i++) {
            array[i] = Integer.parseInt(args[i]);
        }
        return array;
    }
}
